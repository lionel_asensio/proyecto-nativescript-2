import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Color, View } from "tns-core-modules/ui/core/view/view";
import { NoticiasService } from "../domain/noticias.service";
import { Planeta } from "../models/planeta.model";


@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    planetas: Planeta[];
    resultados: Planeta[];
    @ViewChild("layout", { static: false }) layout:ElementRef;

    constructor(private noticias: NoticiasService) {
        this.planetas = [];
    }

    doLater(fn){setTimeout(fn, 1000);}

    ngOnInit(): void {
        this.planetas.push(new Planeta("Mercurio"));
        this.planetas.push(new Planeta("Marte"));
        this.planetas.push(new Planeta("Tierra"));
        this.planetas.forEach(function(x){
            console.log("resultado: " + x.nombre + ", Color: "+ x.color);
        });        
    }
    
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x){
        /*console.dir(x);
        console.dir(x.index);*/
        this.doLater(()=>
            dialogs.action("Elegir Color", "Cancelar!", ["Rojo", "Azul", "Verde"])
               .then((result)=>{
                    if(result != "Cancelar!"){
                        this.planetas[x.index].color = result;
                        console.log("color elegido: " + this.planetas[x.index].color); 
                    }                                      
               })
        );        
    }

    buscarAhora(s: string){
        this.resultados = this.planetas.filter((x)=> x.nombre.indexOf(s) >= 0);
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(()=> layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        }));
    }
}
