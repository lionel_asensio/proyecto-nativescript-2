import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { HomeService } from "../domain/home.service";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor(private home: HomeService) {        
        if( isAndroid){
            this.home.agregar("Bienvenidos a mi applicacion en android");
        }else{
            this.home.agregar("Bienvenidos");
        }
        this.home.agregar("Esta App esta siendo desarrollada para aumentar mis conocimientos.");
        this.home.agregar("En la actualidad es necesario el tener conocimientos en el desarrollo en distintos entornos.");

    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
